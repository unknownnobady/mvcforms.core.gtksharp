﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using Ssepan.Utility.Core;
using Ssepan.Application.Core.GtkSharp;
using Ssepan.Application.Core.GtkSharp.MVC;
using Ssepan.Application.Core.GtkSharp.WinForms;
using Ssepan.Io.Core;
using MvcLibrary.Core;
using Gtk;

namespace MvcForms.Core.GtkSharp
{
    /// <summary>
    /// Note: this class can subclass the base without type parameters.
    /// </summary>
    public class MVCViewModel :
        FormsViewModel<String, MVCSettings, MVCModel, MvcView>
    {
        #region Declarations
        #endregion Declarations

        #region Constructors
        public MVCViewModel() { }//Note: not called, but need to be present to compile--SJS

        public MVCViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<String, String> actionIconImages,
            FileDialogInfo settingsFileDialogInfo
        ) :
            base(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
        }
        #endregion Constructors

        #region Properties
        #endregion Properties

        #region Methods
        /// <summary>
        /// model specific, not generic
        /// </summary>
        public void DoSomething()
        {
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Doing something...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                ModelController<MVCModel>.Model.SomeBoolean = !ModelController<MVCModel>.Model.SomeBoolean;
                ModelController<MVCModel>.Model.SomeInt += 1;
                ModelController<MVCModel>.Model.SomeString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean = !ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
                ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt += 1;
                ModelController<MVCModel>.Model.SomeComponent.SomeOtherString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean = !ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt += 1;
                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString = DateTime.Now.ToString();
                //TODO:implement custom messages
                UpdateStatusBarMessages(null, null, DateTime.Now.ToLongTimeString());

                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar("Did something.");
            }
        }

        //override base
        public override void EditPreferences()
        {
            String errorMessage = null;
            FileDialogInfo fileDialogInfo = null;
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Preferences...",
                    null,
                    _actionIconImages["Preferences"],
                    true,
                    33
                );

                //get folder here
                fileDialogInfo = new FileDialogInfo
                (
                    View,
                    true,
                    "Select Folder...",
                    ResponseType.None
                );
                if (Dialogs.GetFolderPath(ref fileDialogInfo, ref errorMessage))
                {
                    StatusMessage += fileDialogInfo.Filename;
                }
                else
                {
                    StopProgressBar("Select Folder cancelled.");
                    return;
                }

                //override base, which did this
                // if (!Preferences())
                // {
                //     throw new ApplicationException("'Preferences' error");
                // }

                StopProgressBar(StatusMessage);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("Preferences failed: {0}", ex.Message));
            }
        }

        /// <summary>
        /// get font name
        /// </summary>
        public void GetFont()
        {
            String errorMessage = null;
            FontDialogInfo fontDialogInfo = null;
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Get Font...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                //Note:must be set or gets null ref error after dialog closes
                Pango.FontDescription fontDescription = new Pango.FontDescription( );
                fontDialogInfo = new FontDialogInfo
                (
                    View,
                    true,
                    "Select Font",
                    ResponseType.None,
                    fontDescription
                );

                if (Dialogs.GetFont(ref fontDialogInfo, ref errorMessage))
                {
                    if (fontDialogInfo.Response == ResponseType.Ok)
                    {
                        StatusMessage += fontDialogInfo.FontDescription.ToString();
                    }
                    else
                    {
                        StatusMessage += "cancelled";
                    }
                }
                else
                {
                    ErrorMessage = errorMessage;
                }

                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar(StatusMessage);
            }
        }

        /// <summary>
        /// get color RGB
        /// </summary>
        public void GetColor()
        {
            String errorMessage = null;
            ColorDialogInfo colorDialogInfo = null;
            StatusMessage = String.Empty;
            ErrorMessage = String.Empty;

            try
            {
                StartProgressBar
                (
                    "Get Color...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                Gdk.RGBA color = Gdk.RGBA.Zero;
                colorDialogInfo = new ColorDialogInfo
                (
                    View,
                    true,
                    "Select Color",
                    ResponseType.None,
                    color
                );
                
                if (Dialogs.GetColor(ref colorDialogInfo, ref errorMessage))
                {
                    if (colorDialogInfo.Response == ResponseType.Ok)
                    {
                        StatusMessage += 
                            String.Format
                            (
                                "Red:{0},Green:{1},Blue:{2}", 
                                colorDialogInfo.Color.Red.ToString(),
                                colorDialogInfo.Color.Green.ToString(),
                                colorDialogInfo.Color.Blue.ToString()
                            );
                    }
                    else
                    {
                        StatusMessage += "cancelled";
                    }
                }
                else
                {
                    ErrorMessage = errorMessage;
                }


                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                StopProgressBar(null, String.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar(StatusMessage);
            }
        }
        #endregion Methods

    }
}
