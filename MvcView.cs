#define USE_CONFIG_FILEPATH
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Ssepan.Utility.Core;
using Ssepan.Io.Core;
using Ssepan.Application.Core.GtkSharp;
using Ssepan.Application.Core.GtkSharp.MVC;
using MvcLibrary.Core;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;

namespace MvcForms.Core.GtkSharp
{
    /// <summary>
    /// This is the View.
    /// </summary>
    public class MvcView : 
        Window,
        INotifyPropertyChanged
    {
        #region Declarations
        protected Boolean disposed;
        private Boolean _ValueChangedProgrammatically;
        private const String ACTION_INPROGRESS = " ...";
        private const String ACTION_DONE = " done";

        //cancellation hook
        System.Action cancelDelegate = null;
        protected MVCViewModel ViewModel = default(MVCViewModel);

        #region Controls
        [UI] private ImageMenuItem _menuFileNew = null;
        [UI] private ImageMenuItem _menuFileOpen = null;
        [UI] private ImageMenuItem _menuFileSave = null;
        [UI] private ImageMenuItem _menuFileSaveAs = null;
        [UI] private ImageMenuItem _menuFilePrint = null;
        [UI] private ImageMenuItem _menuFilePrintPreview = null;
        [UI] private ImageMenuItem _menuFileQuit = null;
        [UI] private ImageMenuItem _menuEditUndo = null;
        [UI] private ImageMenuItem _menuEditRedo = null;
        [UI] private ImageMenuItem _menuEditSelectAll = null;
        [UI] private ImageMenuItem _menuEditCut = null;
        [UI] private ImageMenuItem _menuEditCopy = null;
        [UI] private ImageMenuItem _menuEditPaste = null;
        [UI] private MenuItem _menuEditPasteSpecial = null;
        [UI] private ImageMenuItem _menuEditDelete = null;
        [UI] private ImageMenuItem _menuEditFind = null;
        [UI] private ImageMenuItem _menuEditReplace = null;
        [UI] private ImageMenuItem _menuEditRefresh = null;
        [UI] private ImageMenuItem _menuEditPreferences = null;
        [UI] private ImageMenuItem _menuHelpContents = null;
        [UI] private MenuItem _menuHelpCheckForUpdates = null;
        [UI] private ImageMenuItem _menuHelpAbout = null;

        [UI] private ToolButton _tbFileNew = null;
        [UI] private ToolButton _tbFileOpen = null;
        [UI] private ToolButton _tbFileSave = null;
        [UI] private ToolButton _tbFileSaveAs = null;
        [UI] private ToolButton _tbFilePrint = null;
        [UI] private ToolButton _tbEditUndo = null;
        [UI] private ToolButton _tbEditRedo = null;
        [UI] private ToolButton _tbEditCut = null;
        [UI] private ToolButton _tbEditCopy = null;
        [UI] private ToolButton _tbEditPaste = null;
        [UI] private ToolButton _tbEditDelete = null;
        [UI] private ToolButton _tbEditFind = null;
        [UI] private ToolButton _tbEditReplace = null;
        [UI] private ToolButton _tbEditRefresh = null;
        [UI] private ToolButton _tbEditPreferences = null;
        [UI] private ToolButton _tbHelpContents = null;

        [UI] private Label _statusMessage = null;
        [UI] private Label _errorMessage = null;
        [UI] private ProgressBar _progressBar = null;
        [UI] private Image _pictureAction = null;
        [UI] private Image _pictureDirty = null;

        [UI] private Button _buttonColor = null;
        [UI] private Button _buttonFont = null;
        [UI] private Label _lblSomeInt = null;
        [UI] private Label _lblSomeOtherInt = null;
        [UI] private Label _lblStillAnotherInt = null;
        [UI] private Label _lblSomeString = null;
        [UI] private Label _lblSomeOtherString = null;
        [UI] private Label _lblStillAnotherString = null;
        [UI] private Label _lblSomeBoolean = null;
        [UI] private Label _lblSomeOtherBoolean = null;
        [UI] private Label _lblStillAnotherBoolean = null;
        [UI] private Entry _txtSomeInt = null;
        [UI] private Entry _txtSomeOtherInt = null;
        [UI] private Entry _txtStillAnotherInt = null;
        [UI] private Entry _txtSomeString = null;
        [UI] private Entry _txtSomeOtherString = null;
        [UI] private Entry _txtStillAnotherString = null;
        [UI] private CheckButton _chkSomeBoolean = null;
        [UI] private CheckButton _chkSomeOtherBoolean = null;
        [UI] private CheckButton _chkStillAnotherBoolean = null;
        [UI] private Button _cmdRun = null;
        #endregion Controls

        private int _counter;
        #endregion Declarations

        #region Constructors    
        public MvcView() : 
            this(new Builder("MvcView.glade")) { }

        private MvcView(Builder builder) : 
            base(builder.GetRawOwnedObject("MvcView"))
        {
            builder.Autoconnect(this);
            #region Events
            this.Realized += Window_Realized;
            this.DeleteEvent += Window_DeleteEvent;
            this.KeyPressEvent += Window_KeyPressEvent;

            _menuFileNew.Activated += MenuFileNew_Activated;
            _menuFileOpen.Activated += MenuFileOpen_Activated;
            _menuFileSave.Activated += MenuFileSave_Activated;
            _menuFileSaveAs.Activated += MenuFileSaveAs_Activated;
            _menuFilePrint.Activated += MenuFilePrintActivated;
            _menuFilePrintPreview.Activated += MenuFilePrintPreview_Activated;
            _menuFileQuit.Activated += MenuFileQuit_Activated;
            _menuEditUndo.Activated += MenuEditUndo_Activated;
            _menuEditRedo.Activated += MenuEditRedo_Activated;
            _menuEditSelectAll.Activated += MenuEditSelectAll_Activated;
            _menuEditCut.Activated += MenuEditCut_Activated;
            _menuEditCopy.Activated += MenuEditCopyActivated;
            _menuEditPaste.Activated += MenuEditPaste_Activated;
            _menuEditPasteSpecial.Activated += MenuEditPasteSpecial_Activated;
            _menuEditDelete.Activated += MenuEditDelete_Activated;
            _menuEditFind.Activated += MenuEditFind_Activated;
            _menuEditReplace.Activated += MenuEditReplace_Activated;
            _menuEditRefresh.Activated += MenuEditRefresh_Activated;
            _menuEditPreferences.Activated += MenuEditPreferences_Activated;
            _menuHelpContents.Activated += MenuHelpContents_Activated;
            _menuHelpCheckForUpdates.Activated += MenuHelpCheckForUpdates_Activated;
            _menuHelpAbout.Activated += MenuHelpAbout_Activated;

            _tbFileNew.Clicked += ToolbarFileNew_Clicked;
            _tbFileOpen.Clicked += ToolbarFileOpen_Clicked;
            _tbFileSave.Clicked += ToolbarFileSave_Clicked;
            _tbFileSaveAs.Clicked += ToolbarFileSaveAs_Clicked;
            _tbFilePrint.Clicked += ToolbarFilePrint_Clicked;
            _tbEditUndo.Clicked += ToolbarEditUndo_Clicked;
            _tbEditRedo.Clicked += ToolbarEditRedo_Clicked;
            _tbEditCut.Clicked += ToolbarEditCut_Clicked;
            _tbEditCopy.Clicked += ToolbarEditCopy_Clicked;
            _tbEditPaste.Clicked += ToolbarEditPaste_Clicked;
            _tbEditDelete.Clicked += ToolbarEditDelete_Clicked;
            _tbEditFind.Clicked += ToolbarEditFind_Clicked;
            _tbEditReplace.Clicked += ToolbarEditReplace_Clicked;
            _tbEditRefresh.Clicked += ToolbarEditRefresh_Clicked;
            _tbEditPreferences.Clicked += ToolbarEditPreferences_Clicked;
            _tbHelpContents.Clicked += ToolbarHelpContents_Clicked;

            _buttonColor.Clicked += ButtonColor_Clicked;
            _buttonFont.Clicked += ButtonFont_Clicked;
            _txtSomeInt.Changed +=  SomeInt_Changed;
            _txtSomeOtherInt.Changed +=  SomeOtherInt_Changed;
            _txtStillAnotherInt.Changed +=  StillAnotherInt_Changed;
            _txtSomeString.Changed +=  SomeString_Changed;
            _txtSomeOtherString.Changed +=  SomeOtherString_Changed;
            _txtStillAnotherString.Changed +=  StillAnotherString_Changed;
            _chkSomeBoolean.Toggled +=  chkSomeBoolean_Toggled;
            _chkSomeOtherBoolean.Toggled +=  chkSomeOtherBoolean_Toggled;
            _chkStillAnotherBoolean.Toggled +=  chkStillAnotherBoolean_Toggled;
            _cmdRun.Clicked += cmdRun_Clicked;
            #endregion Events

            //Not shown in titlebar, but visible in Alt-Tab
            SetIconFromFile("./Resources/App.png");

            _statusMessage.Text = "";
            _errorMessage.Text = "";

                ////(re)define default output delegate
                //ConsoleApplication.defaultOutputDelegate = ConsoleApplication.messageBoxWrapperOutputDelegate;

                //subscribe to view's notifications
                this.PropertyChanged += PropertyChangedEventHandlerDelegate;

                InitViewModel();

                BindSizeAndLocation();

        }
        #endregion Constructors

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(String propertyName)
        {
            try
            {
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                throw;
            }
        }
        #endregion INotifyPropertyChanged

        #region Properties
        private String _ViewName = Program.APP_NAME;
        public String ViewName
        {
            get { return _ViewName; }
            set
            {
                _ViewName = value;
                OnPropertyChanged("ViewName");
            }
        }
        #endregion Properties

        #region Handlers

        #region PropertyChangedEventHandlerDelegates
        /// <summary>
        /// Note: model property changes update UI manually.
        /// Note: handle settings property changes manually.
        /// Note: because settings properties are a subset of the model 
        ///  (every settings property should be in the model, 
        ///  but not every model property is persisted to settings)
        ///  it is decided that for now the settigns handler will 
        ///  invoke the model handler as well.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PropertyChangedEventHandlerDelegate
        (
            Object sender,
            PropertyChangedEventArgs e
        )
        {
            try
            {
                #region Model
                if (e.PropertyName == "IsChanged")
                {
                    //ConsoleApplication.defaultOutputDelegate(String.Format("{0}", e.PropertyName));
                    ApplySettings();
                }
                //Status Bar
                else if (e.PropertyName == "ActionIconIsVisible")
                {
                    _pictureAction.Visible = (ViewModel.ActionIconIsVisible);
                }
                else if (e.PropertyName == "ActionIconImage")
                {
                    _pictureAction.IconName = (ViewModel != null ? ViewModel.ActionIconImage : (String)null);
                }
                else if (e.PropertyName == "StatusMessage")
                {
                    //replace default action by setting control property
                    //skip status message updates after Viewmodel is null
                    _statusMessage.Text = (ViewModel != null ? ViewModel.StatusMessage : (String)null);
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(String.Format("{0}", StatusMessage));
                }
                else if (e.PropertyName == "ErrorMessage")
                {
                    //replace default action by setting control property
                    //skip status message updates after Viewmodel is null
                    _errorMessage.Text = (ViewModel != null ? ViewModel.ErrorMessage : (String)null);
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(String.Format("{0}", ErrorMessage));
                }
                else if (e.PropertyName == "CustomMessage")
                {
                    //replace default action by setting control property
                    //StatusBarCustomMessage.Text = ViewModel.CustomMessage;
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(String.Format("{0}", ErrorMessage));
                }
                else if (e.PropertyName == "ErrorMessageToolTipText")
                {
                    _errorMessage.TooltipText = ViewModel.ErrorMessageToolTipText;
                }
                else if (e.PropertyName == "ProgressBarValue")
                {
                    _progressBar.Fraction = ViewModel.ProgressBarValue / 100;
                }
                else if (e.PropertyName == "ProgressBarMaximum")
                {
                    //TODO:_progressBar.Maximum = ViewModel.ProgressBarMaximum;
                }
                else if (e.PropertyName == "ProgressBarMinimum")
                {
                    //TODO:_progressBar.Minimum = ViewModel.ProgressBarMinimum;
                }
                else if (e.PropertyName == "ProgressBarStep")
                {
                    _progressBar.PulseStep = ViewModel.ProgressBarStep;
                }
                else if (e.PropertyName == "ProgressBarIsMarquee")
                {
                    //TODO:_progressBar.Style = (ViewModel.ProgressBarIsMarquee ? ProgressBarStyle.Marquee : ProgressBarStyle.Blocks);
                }
                else if (e.PropertyName == "ProgressBarIsVisible")
                {
                    if (_progressBar.Visible)
                    {
                        _progressBar.Pulse();
                    }
                    _progressBar.Visible = (ViewModel.ProgressBarIsVisible);
                    if (_progressBar.Visible)
                    {
                        _progressBar.Pulse();
                    }
                }
                else if (e.PropertyName == "DirtyIconIsVisible")
                {
                    _pictureDirty.Visible = (ViewModel.DirtyIconIsVisible);
                }
                else if (e.PropertyName == "DirtyIconImage")
                {
                    _pictureDirty.IconName = ViewModel.DirtyIconImage;
                }
                //use if properties cannot be databound
                else if (e.PropertyName == "SomeInt")
                {
                   _txtSomeInt.Text = ModelController<MVCModel>.Model.SomeInt.ToString();
                }
                else if (e.PropertyName == "SomeBoolean")
                {
                   _chkSomeBoolean.Active = ModelController<MVCModel>.Model.SomeBoolean;
                }
                else if (e.PropertyName == "SomeString")
                {
                   _txtSomeString.Text = ModelController<MVCModel>.Model.SomeString;
                }
                else if (e.PropertyName == "StillAnotherInt")
                {
                   _txtStillAnotherInt.Text = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt.ToString();
                }
                else if (e.PropertyName == "StillAnotherBoolean")
                {
                   _chkStillAnotherBoolean.Active = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
                }
                else if (e.PropertyName == "StillAnotherString")
                {
                   _txtStillAnotherString.Text = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString;
                }
                else if (e.PropertyName == "SomeOtherInt")
                {
                   _txtSomeOtherInt.Text = ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt.ToString();
                }
                else if (e.PropertyName == "SomeOtherBoolean")
                {
                   _chkSomeOtherBoolean.Active = ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
                }
                else if (e.PropertyName == "SomeOtherString")
                {
                   _txtSomeOtherString.Text = ModelController<MVCModel>.Model.SomeComponent.SomeOtherString;
                }
                //else if (e.PropertyName == "SomeComponent")
                //{
                //    ConsoleApplication.defaultOutputDelegate(String.Format("SomeComponent: {0},{1},{2}", ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt, ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean, ModelController<MVCModel>.Model.SomeComponent.SomeOtherString));
                //}
                //else if (e.PropertyName == "StillAnotherComponent")
                //{
                //    ConsoleApplication.defaultOutputDelegate(String.Format("StillAnotherComponent: {0},{1},{2}", ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString));
                //}
                else
                {
                    #if DEBUG_MODEL_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(String.Format("e.PropertyName: {0}", e.PropertyName));
                    #endif
                }
                #endregion Model

                #region Settings
                if (e.PropertyName == "Dirty")
                {
                    //apply settings that don't have databindings
                    ViewModel.DirtyIconIsVisible = (SettingsController<MVCSettings>.Settings.Dirty);
                }
                else
                {
                    #if DEBUG_SETTINGS_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(String.Format("e.PropertyName: {0}", e.PropertyName));
                    #endif
                }
                #endregion Settings
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
        }

        #endregion PropertyChangedEventHandlerDelegates

        #region Form Events
        ///View_Load
        private void Window_Realized(Object sender, EventArgs e)
        {
            try
            {
                ViewModel.StatusMessage = String.Format("{0} starting...", ViewName);
                    
                ViewModel.StatusMessage = String.Format("{0} started.", ViewName);

                _Run();
            }
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                ViewModel.StatusMessage = String.Empty;

                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// handles Window_Delete and (indirectly) FileQuit_Clicked
        /// </summary>
        private void Window_DeleteEvent(Object sender, DeleteEventArgs e)
        {
            Boolean resultDontQuit = false;

            try
            {
                ViewModel.StartProgressBar(_menuFileQuit.TooltipText + ACTION_INPROGRESS,"",null,true,33);
                // StartActionIcon(Gtk.Stock.Undo); //_menuEditUndo.Image.?

                ViewModel.FileExit(ref resultDontQuit);
                e.RetVal = resultDontQuit;
                if (!resultDontQuit)
                {
                    ViewModel.StatusMessage = String.Format("{0} completing...", ViewName);
                    DisposeSettings();
                    ViewModel.StatusMessage = String.Format("{0} completed.", ViewName);

                    ViewModel = null;

                    Application.Quit();
                }
                else
                {
                    
                    ViewModel.StopProgressBar(_menuFileQuit.TooltipText + ACTION_INPROGRESS +" cancelled");
                }

            }
            catch(Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message.ToString();
                ViewModel.StatusMessage = "";

                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
            finally
            {

            }
        }

        ///TODO:handle ProcessCmdKey
        // [ GLib.ConnectBefore ] // need this to allow program to intercept the key first.
        private void Window_KeyPressEvent(Object sender, KeyPressEventArgs e)
        {
        //     Boolean returnValue = default(Boolean);
            try
            {
                
                // Implement the Escape / Cancel keystroke
                // if (keyData == Keys.Cancel || keyData == Keys.Escape)
                // {
                //     //if a long-running cancellable-action has registered 
                //     //an escapable-event, trigger it
                //     InvokeActionCancel();

                //     // This keystroke was handled, 
                //     //don't pass to the control with the focus
                //     returnValue = true;
                // }
                // else
                // {
                //     returnValue = base.ProcessCmdKey(ref msg, keyData);
                // }

            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
        //     return returnValue;
        }

        #endregion Form Events

        #region Control Events
        private void cmdRun_Click(Object sender, EventArgs e)
        {
            ViewModel.DoSomething();
        }

        private void ButtonColor_Clicked(Object sender, EventArgs e)
        {
            ViewModel.GetColor();
        }

        private void ButtonFont_Clicked(Object sender, EventArgs e)
        {
            ViewModel.GetFont();
        }

        private void SomeInt_Changed(Object sender, EventArgs e)
        {
            Int32 result = 0;

            ModelController<MVCModel>.Model.SomeInt = 
                (int.TryParse(_txtSomeInt.Text, out result) ? result : 0);
        }

        private void SomeOtherInt_Changed(Object sender, EventArgs e)
        {
            Int32 result = 0;
            
            ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt = 
                (int.TryParse(_txtSomeOtherInt.Text, out result) ? result : 0);
        }

        private void StillAnotherInt_Changed(Object sender, EventArgs e)
        {
            Int32 result = 0;
            
            ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt = 
                (int.TryParse(_txtStillAnotherInt.Text, out result) ? result : 0);
        }

        private void SomeString_Changed(Object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.SomeString = _txtSomeString.Text;
        }

        private void SomeOtherString_Changed(Object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.SomeComponent.SomeOtherString = _txtSomeOtherString.Text;
        }

        private void StillAnotherString_Changed(Object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString = _txtStillAnotherString.Text;
        }

        private void chkSomeBoolean_Toggled(Object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.SomeBoolean = _chkSomeBoolean.Active;
        }

        private void chkSomeOtherBoolean_Toggled(Object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean = _chkSomeOtherBoolean.Active;
        }

        private void chkStillAnotherBoolean_Toggled(Object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean = _chkStillAnotherBoolean.Active;
        }

        private void cmdRun_Clicked(Object sender, EventArgs e)
        {
            //do something
            ViewModel.DoSomething();
        }


        #endregion Control Events

        #region Menu Events

        private void MenuFileNew_Activated(Object sender, EventArgs e)
        {
            ViewModel.FileNew();
        }

        private void MenuFileOpen_Activated(Object sender, EventArgs e)
        {
            ViewModel.FileOpen();
        }

        private void MenuFileSave_Activated(Object sender, EventArgs e)
        {
            ViewModel.FileSave();
        }

        private void MenuFileSaveAs_Activated(Object sender, EventArgs e)
        {
            ViewModel.FileSaveAs();
        }

        private void MenuFilePrintActivated(Object sender, EventArgs e)
        {
            ViewModel.FilePrint();
        }

        private void MenuFilePrintPreview_Activated(Object sender, EventArgs e)
        {
            ViewModel.FilePrintPreview();
        }

        /// <summary>
        /// triggers Window_Delete which handles prompting user,
        /// and cancelling or quitting
        /// </summary>
        private void MenuFileQuit_Activated(Object sender, EventArgs e)
        {
            this.Close();
        }

        private void MenuEditUndo_Activated(Object sender, EventArgs e)
        {
            ViewModel.EditUndo();
        }

        private  void MenuEditRedo_Activated(Object sender, EventArgs e)
        {
            ViewModel.EditRedo();
        }

        private  void MenuEditSelectAll_Activated(Object sender, EventArgs e)
        {
            ViewModel.EditSelectAll();
        }

        private  void MenuEditCut_Activated(Object sender, EventArgs e)
        {
            ViewModel.EditCut();
        }

        private  void MenuEditCopyActivated(Object sender, EventArgs e)
        {
            ViewModel.EditCopy();
        }

        private  void MenuEditPaste_Activated(Object sender, EventArgs e)
        {
            ViewModel.EditPaste();
        }

        private  void MenuEditPasteSpecial_Activated(Object sender, EventArgs e)
        {
            //ViewModel.PasteSpecial();
        }

        private  void MenuEditDelete_Activated(Object sender, EventArgs e)
        {
            ViewModel.EditDelete();
        }

        private  void MenuEditFind_Activated(Object sender, EventArgs e)
        {
            ViewModel.EditFind();
        }

        private  void MenuEditReplace_Activated(Object sender, EventArgs e)
        {
            ViewModel.EditReplace();
        }

        private  void MenuEditRefresh_Activated(Object sender, EventArgs e)
        {
            ViewModel.EditRefresh();
        }

        private void MenuEditPreferences_Activated(Object sender, EventArgs e)
        {
            ViewModel.EditPreferences();
        }

        private  void MenuHelpContents_Activated(Object sender, EventArgs e)
        {
            ViewModel.HelpContents();
        }
        private  void MenuHelpCheckForUpdates_Activated(Object sender, EventArgs e)
        {
            ViewModel.HelpCheckForUpdates();
        }
        private void MenuHelpAbout_Activated(Object sender, EventArgs e)
        {
            ViewModel.HelpAbout<AssemblyInfo>();
        }
        #endregion Menu Events

        #region Toolbar Events
        private  void ToolbarFileNew_Clicked(Object sender, EventArgs e)
        {
            ViewModel.FileNew();
        }
        private void ToolbarFileOpen_Clicked(Object sender, EventArgs e)
        {
            ViewModel.FileOpen();
        }
        private void ToolbarFileSave_Clicked(Object sender, EventArgs e)
        {
            ViewModel.FileSave();
        }
        private void ToolbarFileSaveAs_Clicked(Object sender, EventArgs e)
        {
            ViewModel.FileSaveAs();
        }
        private void ToolbarFilePrint_Clicked(Object sender, EventArgs e)
        {
            ViewModel.FilePrint();
        }
        private  void ToolbarEditUndo_Clicked(Object sender, EventArgs e)
        {
            ViewModel.EditUndo();
        }
        private  void ToolbarEditRedo_Clicked(Object sender, EventArgs e)
        {
            ViewModel.EditRedo();
        }
        private  void ToolbarEditCut_Clicked(Object sender, EventArgs e)
        {
            ViewModel.EditCut();
        }
        private  void ToolbarEditCopy_Clicked(Object sender, EventArgs e)
        {
            ViewModel.EditCopy();
        }
        private  void ToolbarEditPaste_Clicked(Object sender, EventArgs e)
        {
            ViewModel.EditPaste();
        }
        private  void ToolbarEditDelete_Clicked(Object sender, EventArgs e)
        {
            ViewModel.EditDelete();
        }
        private  void ToolbarEditFind_Clicked(Object sender, EventArgs e)
        {
            ViewModel.EditFind();
        }
        private  void ToolbarEditReplace_Clicked(Object sender, EventArgs e)
        {
            ViewModel.EditReplace();
        }
        private  void ToolbarEditRefresh_Clicked(Object sender, EventArgs e)
        {
            ViewModel.EditRefresh();
        }
        private void ToolbarEditPreferences_Clicked(Object sender, EventArgs e)
        {
            ViewModel.EditPreferences();
        }
        private  void ToolbarHelpContents_Clicked(Object sender, EventArgs e)
        {
            ViewModel.HelpContents();
        }
        #endregion Toolbar Events

        #endregion Handlers


        #region Methods
        #region FormAppBase
        protected void InitViewModel()
        {
            try
            {
                //tell controller how model should notify view about non-persisted properties AND including model properties that may be part of settings
                ModelController<MVCModel>.DefaultHandler = PropertyChangedEventHandlerDelegate;

                //tell controller how settings should notify view about persisted properties
                SettingsController<MVCSettings>.DefaultHandler = PropertyChangedEventHandlerDelegate;

                InitModelAndSettings();

                FileDialogInfo settingsFileDialogInfo =
                    new FileDialogInfo
                    (
                        this,//parent
                        true,//modal
                        null,//title
                        ResponseType.None,
                        SettingsController<MVCSettings>.FILE_NEW,//newfilename
                        null,//filename
                        MVCSettings.FileTypeExtension,//FileTypeExtension
                        MVCSettings.FileTypeDescription,//FileTypeDescription
                        MVCSettings.FileTypeName,//FileTypeName
                        new String[] 
                        { 
                            "XML files (*.xml)|*.xml", 
                            "All files (*.*)|*.*" 
                        },//AdditionalFilters
                        false,//multiselect
                        default(Environment.SpecialFolder),//initialDirectory
                        false,//forcedialog
                        false,//forcenew
                        Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()//customInitialDirectory
                    );

                //set dialog caption
                settingsFileDialogInfo.Title = this.Title;

                //class to handle standard behaviors
                ViewModelController<String, MVCViewModel>.New
                (
                    ViewName,
                    new MVCViewModel
                    (
                        this.PropertyChangedEventHandlerDelegate,
                        new Dictionary<String, String>() 
                        { //TODO:ideally, should get these from library, but items added did not get generated into resource class.
                            { "New", Gtk.Stock.New }, 
                            { "Open", Gtk.Stock.Open },
                            { "Save", Gtk.Stock.Save },
                            { "SaveAs", Gtk.Stock.SaveAs },
                            { "Print", Gtk.Stock.Print },
                            { "PrintPreview", Gtk.Stock.PrintPreview },
                            { "Quit", Gtk.Stock.Quit },
                            { "Undo", Gtk.Stock.Undo },
                            { "Redo", Gtk.Stock.Redo },
                            { "SelectAll", Gtk.Stock.SelectAll },
                            { "Cut", Gtk.Stock.Cut },
                            { "Copy", Gtk.Stock.Copy },
                            { "Paste", Gtk.Stock.Paste },
                            { "Delete", Gtk.Stock.Delete },
                            { "Find", Gtk.Stock.Find },
                            { "Replace", Gtk.Stock.FindAndReplace },
                            { "Refresh", Gtk.Stock.Refresh },
                            { "Preferences", Gtk.Stock.Preferences },
                            { "Properties", Gtk.Stock.Properties },
                            { "Contents", Gtk.Stock.Help },
                            { "About", Gtk.Stock.About }
                        },
                        settingsFileDialogInfo
                    )
                );

                //select a viewmodel by view name
                ViewModel = ViewModelController<String, MVCViewModel>.ViewModel[ViewName];

                BindFormUi();

                //Init config parameters
                if (!LoadParameters())
                {
                    throw new Exception(String.Format("Unable to load config file parameter(s)."));
                }

                //DEBUG:filename coming in is being converted/passed as DOS 8.3 format equivalent
                //Load
                if ((SettingsController<MVCSettings>.FilePath == null) || (SettingsController<MVCSettings>.Filename == SettingsController<MVCSettings>.FILE_NEW))
                {
                    //NEW
                    ViewModel.FileNew();
                }
                else
                {
                    //OPEN
                    ViewModel.FileOpen(false);
                }

#if debug
            //debug view
            menuEditProperties_Click(sender, e);
#endif

                //Display dirty state
                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                if (ViewModel != null)
                {
                    ViewModel.ErrorMessage = ex.Message;
                }
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
        }

        protected void InitModelAndSettings()
        {
            //create Settings before first use by Model
            if (SettingsController<MVCSettings>.Settings == null)
            {
                SettingsController<MVCSettings>.New();
            }
            //Model properties rely on Settings, so don't call Refresh before this is run.
            if (ModelController<MVCModel>.Model == null)
            {
                ModelController<MVCModel>.New();
            }
        }

        protected void DisposeSettings()
        {
            MessageDialogInfo questionMessageDialogInfo = null;
            String errorMessage = null;

            //save user and application settings 
            Properties.Settings.Default.Save();

            if (SettingsController<MVCSettings>.Settings.Dirty)
            {
                //prompt before saving
                questionMessageDialogInfo = new MessageDialogInfo
                (
                    this,
                    true,
                    this.Title,
                    DialogFlags.DestroyWithParent,
                    MessageType.Question,
                    ButtonsType.YesNo,
                    "Save changes?",
                    ResponseType.None
                );
                if (!Dialogs.ShowMessageDialog(ref questionMessageDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(errorMessage);
                }

                // DialogResult dialogResult = MessageBox.Show("Save changes?", this.Title, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                switch (questionMessageDialogInfo.Response)
                {
                    case ResponseType.Yes:
                        {
                            //SAVE
                            ViewModel.FileSave();

                            break;
                        }
                    case ResponseType.No:
                        {
                            break;
                        }
                    default:
                        {
                            throw new InvalidEnumArgumentException();
                        }
                }
            }

            //unsubscribe from model notifications
            ModelController<MVCModel>.Model.PropertyChanged -= PropertyChangedEventHandlerDelegate;
        }

        protected void _Run()
        {
            //MessageBox.Show("running", "MVC", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }
        #endregion FormAppBase

        #region Utility
        /// <summary>
        /// Bind static Model controls to Model Controller
        /// </summary>
        private void BindFormUi()
        {
            try
            {
                //Form

                //Controls
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Bind Model controls to Model Controller
        /// Note: databinding not available in GtkSharp, but leave tihs in place 
        ///  in case I figure out a way to do this
        /// </summary>
        private void BindModelUi()
        {
            try
            {
                BindField<Entry, MVCModel>(_txtSomeInt, ModelController<MVCModel>.Model, "SomeInt");
                BindField<Entry, MVCModel>(_txtSomeString, ModelController<MVCModel>.Model, "SomeString");
                BindField<CheckButton, MVCModel>(_chkSomeBoolean, ModelController<MVCModel>.Model, "SomeBoolean", "Checked");

                BindField<Entry, MVCModel>(_txtSomeOtherInt, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherInt");
                BindField<Entry, MVCModel>(_txtSomeOtherString, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherString");
                BindField<CheckButton, MVCModel>(_chkSomeOtherBoolean, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherBoolean", "Checked");
                
                BindField<Entry, MVCModel>(_txtStillAnotherInt, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherInt");
                BindField<Entry, MVCModel>(_txtStillAnotherString, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherString");
                BindField<CheckButton, MVCModel>(_chkStillAnotherBoolean, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherBoolean", "Checked");
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// Note: databinding not available in GtkSharp, but leave this in place 
        ///  in case I figure out a way to do this
        private void BindField<TControl, TModel>
        (
            TControl fieldControl,
            TModel model,
            String modelPropertyName,
            String controlPropertyName = "Text",
            Boolean formattingEnabled = false,
            //DataSourceUpdateMode dataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged,
            Boolean reBind = true
        )
            where TControl : Gtk.Widget
        {
            try
            {
                //TODO: .RemoveSignalHandler ?
                //fieldControl.DataBindings.Clear();
                if (reBind)
                {
                    //TODO:.AddSignalHandler ?
                    //fieldControl.DataBindings.Add(controlPropertyName, model, modelPropertyName, formattingEnabled, dataSourceUpdateMode);
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Apply Settings to viewer.
        /// </summary>
        private void  ApplySettings()
        {
            try
            {
                _ValueChangedProgrammatically = true;

                //apply settings that have databindings
                BindModelUi();

                //apply settings that shouldn't use databindings

                //apply settings that can't use databindings
                Title = System.IO.Path.GetFileName(SettingsController<MVCSettings>.Filename) + " - " + ViewName;

                //apply settings that don't have databindings
                ViewModel.DirtyIconIsVisible = (SettingsController<MVCSettings>.Settings.Dirty);
                //update remaining non-bound controls: textboxes, checkboxes
                //Note: ModelController<MVCModel>.Model.Refresh() will cause SO here
                ModelController<MVCModel>.Model.Refresh("SomeInt");
                ModelController<MVCModel>.Model.Refresh("SomeBoolean");
                ModelController<MVCModel>.Model.Refresh("SomeString");
                ModelController<MVCModel>.Model.Refresh("SomeOtherInt");
                ModelController<MVCModel>.Model.Refresh("SomeOtherBoolean");
                ModelController<MVCModel>.Model.Refresh("SomeOtherString");
                ModelController<MVCModel>.Model.Refresh("StillAnotherInt");
                ModelController<MVCModel>.Model.Refresh("StillAnotherBoolean");
                ModelController<MVCModel>.Model.Refresh("StillAnotherString");

                _ValueChangedProgrammatically = false;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Set function button and menu to enable value, and cancel button to opposite.
        /// For now, do only disabling here and leave enabling based on biz logic 
        ///  to be triggered by refresh?
        /// </summary>
        /// <param name="functionButton"></param>
        /// <param name="functionMenu"></param>
        /// <param name="cancelButton"></param>
        /// <param name="enable"></param>
        private void SetFunctionControlsEnable
        (
            Button functionButton,
            ToolButton functionToolbarButton,
            ImageMenuItem functionMenu,
            Button cancelButton,
            Boolean enable
        )
        {
            try
            {
                //stand-alone button
                if (functionButton != null)
                {
                    functionButton.Sensitive = enable;
                }

                //toolbar button
                if (functionToolbarButton != null)
                {
                    functionToolbarButton.Sensitive = enable;
                }

                //menu item
                if (functionMenu != null)
                {
                    functionMenu.Sensitive = enable;
                }

                //stand-alone cancel button
                if (cancelButton != null)
                {
                    cancelButton.Sensitive = !enable;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Invoke any delegate that has been registered 
        ///  to cancel a long-running background process.
        /// </summary>
        private void InvokeActionCancel()
        {
            try
            {
                //execute cancellation hook
                if (cancelDelegate != null)
                {
                    cancelDelegate();
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Load from app config; override with command line if present
        /// </summary>
        /// <returns></returns>
        private Boolean LoadParameters()
        {
            Boolean returnValue = default(Boolean);
#if USE_CONFIG_FILEPATH
            String _settingsFilename = default(String);
#endif

            try
            {
                if ((Program.Filename != null) && (Program.Filename != SettingsController<MVCSettings>.FILE_NEW))
                {
                    //got filename from command line
                    //DEBUG:filename coming in is being converted/passed as DOS 8.3 format equivalent
                    // if (!RegistryAccess.ValidateFileAssociation(Application.ExecutablePath, "." + MVCSettings.FileTypeExtension))
                    // {
                    //     throw new ApplicationException(String.Format("Settings filename not associated: '{0}'.\nCheck filename on command line.", Program.Filename));
                    // }
                    //it passed; use value from command line
                }
                else
                {
#if USE_CONFIG_FILEPATH
                    //get filename from app.config
                    if (!Configuration.ReadString("SettingsFilename", out _settingsFilename))
                    {
                        throw new ApplicationException(String.Format("Unable to load SettingsFilename: {0}", "SettingsFilename"));
                    }
                    if ((_settingsFilename == null) || (_settingsFilename == SettingsController<MVCSettings>.FILE_NEW))
                    {
                        throw new ApplicationException(String.Format("Settings filename not set: '{0}'.\nCheck SettingsFilename in app config file.", _settingsFilename));
                    }
                    //use with the supplied path
                    SettingsController<MVCSettings>.Filename = _settingsFilename;

                    if (System.IO.Path.GetDirectoryName(_settingsFilename) == String.Empty)
                    {
                        //supply default path if missing
                        SettingsController<MVCSettings>.Pathname = Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator();
                    }
#endif
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                //throw;
            }
            return returnValue;
        }

        private void BindSizeAndLocation()
        {

            //Note:Size must be done after InitializeComponent(); do Location this way as well.--SJS
            // this.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::MVCForms.Properties.Settings.Default, "Location", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            // this.DataBindings.Add(new System.Windows.Forms.Binding("ClientSize", global::MVCForms.Properties.Settings.Default, "Size", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.SetDefaultSize(global::MvcForms.Core.GtkSharp.Properties.Settings.Default.Size.Width,global::MvcForms.Core.GtkSharp.Properties.Settings.Default.Size.Height);
            this.SetPosition(WindowPosition.Center); // global::MVCForms.Properties.Settings.Default.Location;
        }
        #endregion Utility

        #endregion Methods


        #region Actions
        private async Task DoSomething()
        {
            await Task.Delay(3000); 
        }

        #endregion Actions

    }
}
