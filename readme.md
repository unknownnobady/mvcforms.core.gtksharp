readme.md - README for application
MvcForms.Core.GtkSharp v0.4

Desktop GUI app prototype, on Linux, in C# / DotNet[5|6] / GtkSharp, using VSCode / Glade.

Purpose:
To illustrate a working Desktop GUI app in c# on .Net (Core). Uses GtkSharp where WinForms was being used.


Usage notes:
~...
Instructions for downloading/installing .Net 'Core':
https://dotnet.microsoft.com/download
https://docs.microsoft.com/en-us/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website

Instructions/example of adding GtkSharp to dotnet and creating app ui in Glade:
https://yasar-yy.medium.com/cross-platform-desktop-gui-application-using-net-core-5894b0ad89a8

GUI tutorial for Gtk#/C#:
https://zetcode.com/gui/gtksharp/



Enhancements:
0.4:
-Convert MVCForms app(s) and library, along with support libraries, to Core. Where WinForms was involved, convert that project to GtkSharp instead.
